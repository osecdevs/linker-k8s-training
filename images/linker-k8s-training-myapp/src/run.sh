#!/bin/bash
if [ -z ${EMOTION+x} ]; then
	echo "ERROR: \$EMOTION is not set. Exiting."
	exit 1; 
fi

if [ -z ${CONFIG_LOCATION+x} ]; then
	echo "ERROR: \$CONFIG_LOCATION is not set. Exiting."
	exit 1; 
fi


if [ ! -f ${CONFIG_LOCATION} ] ; then
	echo "ERROR: Can't load ${CONFIG_LOCATION}. Exiting."
	exit 1
fi

echo "myapp starting (with ${EMOTION})"
echo "loading config from: ${CONFIG_LOCATION}"

. ${CONFIG_LOCATION}

if [ -z ${foo+x} ]; then
	echo "ERROR: \$foo is not set. Exiting."
	exit 1; 
fi

echo "The value of foo is ${foo}."

while true ; do
	echo "I'm alive (will print this every 5 minutes)."
	sleep 300
done


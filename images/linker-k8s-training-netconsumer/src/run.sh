#!/bin/bash
DATADIR=/data

if [ ! -d $DATADIR ] ; then
        mkdir -p $DATADIR
fi

nc -l 5060 -v -k -c "/bin/jq -r .key | sed -e 's/\///' | xargs -I {} touch ${DATADIR}/{}"

